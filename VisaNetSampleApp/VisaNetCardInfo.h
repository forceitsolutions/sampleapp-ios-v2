//
//  VisaNetCardInfo.h
//  VisaNetLib
//
//  Created by AlexWang on 6/22/15.
//  Copyright (c) 2015 VisaNet. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VisaNetCardInfo : NSObject

@property (strong, nonatomic) NSString *cardAlias;
@property (strong, nonatomic) NSString *lastDigits;
@property (strong, nonatomic) NSString *cardTokenId;
@property (nonatomic) BOOL cvvActive;
@property (nonatomic) BOOL isSelected;

@end
