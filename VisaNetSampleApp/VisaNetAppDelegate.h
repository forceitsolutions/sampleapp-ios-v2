//
//  VisaNetAppDelegate.h
//  VisaNetSampleApp
//
//  Created by Giancarlo Gallardo on 9/3/14.
//  Copyright (c) 2014 QuipuTech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VisaNetAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
