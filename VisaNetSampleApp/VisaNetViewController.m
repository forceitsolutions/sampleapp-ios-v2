//
//  VisaNetViewController.m
//  VisaNetSampleApp
//
//  Created by Giancarlo Gallardo on 9/3/14.
//  Copyright (c) 2014 QuipuTech. All rights reserved.
//

#import "VisaNetViewController.h"
#import "VisaNetConfigurationContext.h"
#import "VisaNetPaymentViewController.h"
#import "TokenizationClient.h"

#import "VisaNetPaymentDismissAnimationController.h"
#import "VisaNetPaymentPresentAnimationController.h"
#import "LoadingActivity.h"

@interface VisaNetViewController () <UIViewControllerTransitioningDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UIStepper *stepAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrency;
@property (weak, nonatomic) IBOutlet UISwitch *swtUseToken;
@property (weak, nonatomic) IBOutlet UISwitch *swtIsTesting;
@property (weak, nonatomic) IBOutlet UISwitch *swtIsSoles;
@end

@implementation VisaNetViewController

- (void) displayAmount {

    double amount = self.stepAmount.value;
    self.lblAmount.text = [NSString stringWithFormat:@"%.02f", amount];
    self.lblCurrency.text = @"S/.";

    /*
    switch (self.segCurrency.selectedSegmentIndex) {
        case 0: // PEN
            self.lblAmount.text = [NSString stringWithFormat:@"%.02f", amount];
            self.lblCurrency.text = @"S/.";
            break;
            
        case 1: // USD
            self.lblAmount.text = [NSString stringWithFormat:@"%.02f", amount];
            self.lblCurrency.text = @"USD";
            break;
    }
     */

    
}

- (IBAction)stepperAmountValueChanged:(UIStepper *)sender
{
    [self displayAmount];
}

- (IBAction)doTouch:(id)sender {
    [[self view] endEditing:true];
}

- (IBAction)doPay:(UIButton *)sender
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
    NSNumber *amount = [numberFormatter numberFromString:self.lblAmount.text];
    NSMutableDictionary *merchantDefinedData = [[NSMutableDictionary alloc] initWithObjectsAndKeys: @"movil", @"field3", // mandatory
        nil];
    
    VisaNetConfigurationContext *context = [[VisaNetConfigurationContext alloc] init];
    context.customerEmail = @"innovacionvndp@gmail.com";
    context.customerFirstName = @"Innovacion";
    context.customerLastName = @"VisaNet";
    context.amount = amount;
    context.isTesting = self.swtIsTesting.on;
    
    NSString *merchantId = self.swtIsTesting.on ? @"521040502" : @"341198210";
    NSString *userTokenId = self.swtUseToken.on ? (self.swtIsTesting.on ? @"269c8764-2f39-453c-b11f-bbad462ecd71" : @"41f4712e-3821-4e70-914d-d92b61b4cba1") : nil;
    NSString *endpoint = @"https://devapi.vnforapps.com/api.tokenization/api/v2";
    NSString *accessKeyId = self.swtIsTesting.on ? @"AKIAI4VNT2CSVWSTJV6Q"
    : @"AKIAI737YRU5WIQ5W6JQ";
    NSString *secretAccessKey = self.swtIsTesting.on ? @"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"
    : @"hssNV8/TJHGs2FQUYTrufGmu/nzudtXU9fPOj5CO";
    
    
    NSString* transactionId = [self getNextCounterForMerchant:merchantId withAccessKeyId:accessKeyId withSecretAccessKey:secretAccessKey withEndPoint:endpoint];
    
    /*
    [LoadingActivity loadActivity:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
    
        transactionId = [self getNextCounterForMerchant:merchantId withAccessKeyId:accessKeyId withSecretAccessKey:secretAccessKey withEndPoint:endpoint];

        
        dispatch_sync(dispatch_get_main_queue(), ^{
            [LoadingActivity finishActivity:self.view];
        });
        
    
    });
    */
    
    
    
    
    context.merchantId = merchantId;
    // pass userToken if needed
    context.userTokenId = userTokenId;
    // pass security information
    context.accessKeyId = accessKeyId;
    context.secretAccessKey = secretAccessKey;
    // pass unique purchase number per operation
    context.transactionId = transactionId;
    // pass if you need testing endpoint or not
    // set currency for transaction
    context.currency = self.swtIsSoles.on ? PEN : USD;
    // set merchant defined data
    context.merchantDefinedData = merchantDefinedData;
    // set endpoint for tokenization client
    [TokenizationClient setEndpoint: context.isTesting ?
     @"https://devapi.vnforapps.com/api.tokenization/api/v2" :
     @"https://api.vnforapps.com/api.tokenization/api/v2"];

    // execute call to library to check for cards
    [LoadingActivity loadActivity:self.view];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0),^{
        
        // check for cards
        NSMutableArray* cards = [TokenizationClient
                                 queryUserToken:context.userTokenId
                                 forMerchant:context.merchantId
                                 withAccessKeyId:context.accessKeyId
                                 withSecretAccessKey:context.secretAccessKey
                                 withTimeout:20000];
        
        
        dispatch_sync(dispatch_get_main_queue(), ^{
            
            [LoadingActivity finishActivity:self.view];
            
            // if there are existing cards available for specified userToken
            if(cards && [cards count] > 0)
            {
                // pass cards to context
                context.cardArray = cards;
                // if you want to support up to N cards
                //context.maxCardsPerToken = [NSNumber numberWithInteger:2];
                cardListController = [[VisaNetCardListViewController alloc] initWithConfigurationContext:context];
                cardListController.delegate = self;
                cardListController.modalPresentationStyle = UIModalPresentationCustom;
                cardListController.transitioningDelegate = self;
                [self presentViewController:cardListController animated:YES completion:nil];
            }
            else
                // if you need to go to the payment form directly
            {
                //
                paymentController = [[VisaNetPaymentViewController alloc] initWithConfigurationContext:context];
                paymentController.delegate = self;
                paymentController.modalPresentationStyle = UIModalPresentationCustom;
                paymentController.transitioningDelegate = self;
                [self presentViewController:paymentController animated:YES completion:nil];
            }
        });
        
    });
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source
{
    return [[VisaNetPaymentPresentAnimationController alloc] init];
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed
{
    return [[VisaNetPaymentDismissAnimationController alloc] init];
}

#pragma mark VisaNetPaymentViewControllerDelegate

- (void)paymentDidCancel
{
    //To Implement your own code here
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)paymendDidComplete:(VisaNetPaymentInfo *)paymentInfo
{
    NSLog(@"Returning from Form");
    
    //To Implement your own code here.
    NSLog(@"%@", [paymentInfo transactionId]);
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"%@", [paymentInfo paymentDescription]);

    for(NSString *key in [[paymentInfo data] allKeys]) {
        NSLog(@"%@: %@", key, [[paymentInfo data] valueForKey:key]);
    }

    NSLog(@"Display result");
    NSLog(@"%@", [paymentInfo email]);
    NSLog(@"%@ %@", [paymentInfo firstName], [paymentInfo lastName]);
    
    NSString* status = [paymentInfo paymentStatus];
    if([status isEqual: @"0"]) {

        NSString *message = @"";
        for (NSString *key in [paymentInfo.data allKeys]) {
            message = [message stringByAppendingString:[NSString stringWithFormat:@"%@: %@\n", key, [paymentInfo.data valueForKey:key]]];
        }
        
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Operación Aprobada" message:message delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Operación Denegada" message:[paymentInfo paymentDescription] delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (NSString *) getNextCounterForMerchant:(NSString *)merchantId
                         withAccessKeyId: (NSString *) accessKeyId
                     withSecretAccessKey: (NSString *) secretAccessKey
                            withEndPoint: (NSString *) endpoint
{
    NSURL *url = [[NSURL alloc] initWithString:[NSString stringWithFormat:@"%@/merchant/%@/nextCounter", endpoint, merchantId]];
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval: 30];
    
    NSString* credentials = [NSString stringWithFormat:@"%@:%@", accessKeyId, secretAccessKey];
    NSData* credentialsData = [credentials dataUsingEncoding:NSUTF8StringEncoding];
    NSString* credentialsBase64 = [NSString stringWithFormat:@"Basic %@", [credentialsData base64EncodedStringWithOptions:0]];
    
    [urlRequest setHTTPMethod:@"GET"];
    [urlRequest setValue:credentialsBase64 forHTTPHeaderField:@"Authorization"];
    
    
    NSHTTPURLResponse *urlResponse;
    NSError *error;
    
    NSData *jsonOutput = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&urlResponse error:&error];
    
    
    if(!urlResponse) {
        NSLog(@"%@", [error description]);
        return nil;
    } else {
        NSLog(@"Status Code: %ld", (long)[urlResponse statusCode]);
        NSString *nextCounter = [[NSString alloc] initWithData:jsonOutput encoding:NSUTF8StringEncoding];
        NSLog(@"Response: \n%@", nextCounter);
        return nextCounter;
    }
}

@end
