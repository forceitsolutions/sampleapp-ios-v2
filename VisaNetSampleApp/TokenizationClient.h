//
//  TokenizationClient.h
//  VisaNetLib
//
//  Created by Giancarlo Gallardo on 8/8/15.
//  Copyright (c) 2015 VisaNet. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CardTransactionRequest.h"
#import "VisaNetPaymentInfo.h"

@interface TokenizationClient : NSObject

+ (void) setEndpoint: (NSString *) newEndpoint;

+ (NSMutableArray *) queryUserToken: (NSString *) userToken
                        forMerchant: (NSString *) merchantId
                    withAccessKeyId: (NSString *) accessKeyId
                withSecretAccessKey: (NSString *) secretAccessKey
                        withTimeout: (double) timeout;

+ (VisaNetPaymentInfo *) executeTransactionWith: (CardTransactionRequest *) cardRequest
                                withAccessKeyId: (NSString *) accessKeyId
                            withSecretAccessKey: (NSString *) secretAccessKey
                                    withTimeout: (double) timeout;

+ (BOOL) deleteCardOnUserToken: (NSString *) userToken
                      andAlias: (NSString *) alias
                   forMerchant: (NSString *) merchantId
               withAccessKeyId: (NSString *) accessKeyId
           withSecretAccessKey: (NSString *) secretAccessKey
                   withTimeout: (double) timeout;
@end
