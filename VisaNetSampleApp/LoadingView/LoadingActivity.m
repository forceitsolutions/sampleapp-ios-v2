//
//  LoadingActivity.m
//  E-stateBook
//
//  Created by AlexWang on 2/15/15.
//  Copyright (c) 2015 TechTroll. All rights reserved.
//

#import "LoadingActivity.h"
#import "RSLoadingView.h"

@implementation LoadingActivity

+ (void)loadActivity:(UIView*)view
{
    RSLoadingView *loadingView = [[RSLoadingView alloc] init];
    [loadingView setFrame:view.bounds];
    [loadingView setAlpha:1.0];
    //[loadingView.titleLabel setText:@"Sending..."];
    [view addSubview:loadingView];
}
+ (void)finishActivity:(UIView*)parent
{
    RSLoadingView *loadView;
    
    for(UIView *view in parent.subviews)
    {
        if([view isKindOfClass:[RSLoadingView class]])
        {
            loadView = (RSLoadingView*)view;
            [loadView removeFromSuperview];
        }
    }
}

@end
