# README #

### 2.0.31 ###
* Soporte para Tokenizar Tarjetas de Crédito


### 1.1 ###
* Actualización de librería TrustDefender a versión 2.6
* Se agregaron 3 nuevos campos en la respuesta para devolver la información ingresada en el formulario
* Nombres: data[form.firstName]
* Apellidos: data[form.lastName]
* Correo electrónico: data[form.email]


### 1.0.11 ###
* Cambio en logo Visa

### 1.0.10 ###
* Se agregó control de parámetros merchantId y transactionId (regexp)

### 1.0.9 ###


* Fix de corrección de bug al hacer llamada asíncrona.