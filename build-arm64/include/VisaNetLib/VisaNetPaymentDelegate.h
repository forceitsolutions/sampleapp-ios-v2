//
//  VisaNetPaymentDelegate.h
//  VisaNetLib
//
//  Created by Giancarlo Gallardo on 8/9/15.
//  Copyright (c) 2015 VisaNet. All rights reserved.
//

#ifndef VisaNetLib_VisaNetPaymentDelegate_h
#define VisaNetLib_VisaNetPaymentDelegate_h

@protocol VisaNetPaymentDelegate <NSObject>
@required

- (void) paymentDidCancel;
- (void) paymendDidComplete: (VisaNetPaymentInfo *) paymentInfo;

@end

#endif
